 -- 1. Return the customerName of the customers who are from the Philippines
   SELECT customerName FROM customers WHERE country = 'Philippines';
 -- 2. Return the contactLastName and contactFirstName of customers with name "La Rochelle Gifts"
   SELECT contactLastName, contactFirstName FROM customers WHERE customerName = "La Rochelle Gifts";
 -- 3. Return the product name and MSRP of the product named "The Titanic"
   SELECT productName, MSRP FROM products WHERE productName = "The Titanic";
 -- 4. Return the first and last name of the employee whose email is "jfirrelli@classicmodelcars.com"
   SELECT firstName, lastName FROM employees WHERE email = "jfirrelli@classicmodelcars.com";
 -- 5. Return the names of customers who have no registered state
   SELECT customerName from customers WHERE state IS NULL;
 -- 6. Return the first name, last name, email of the employee whose last name is Patterson and first name is Steve
   SELECT firstName, lastName, email FROM employees WHERE lastName = "Patterson" AND firstName ="Steve";
 -- 7. Return customer name, country, and credit limit of customers whose countries are NOT USA and whose credit limits are greater than 3000
   SELECT customerName, country, creditLimit FROM customers WHERE country != "USA" AND creditLimit > 3000;
 -- 8. Return the customer names of customers whose customer names don't have 'a' in them
   SELECT customerName FROM customers WHERE customerName NOT LIKE "%a%";
 -- 9. Return the customer numbers of orders whose comments contain the string 'DHL'
   SELECT customerNumber FROM orders WHERE comments LIKE "%DHL%";
 -- 10. Return the product lines whose text description mentions the phrase 'state of the art'
   SELECT productLine FROM productlines WHERE textDescription LIKE "%state of the art%";
 -- 11. Return the countries of customers without duplication
   SELECT country FROM customers GROUP BY country;
 -- 12. Return the statuses of orders without duplication
   SELECT status FROM orders GROUP BY status;
 -- 13. Return the customer names and countries of customers whose country is USA, France, or Canada
   SELECT customerName, country FROM customers WHERE country="USA" OR country="France" OR country="Canada";
 -- 14. Return the first name, last name, and office's city of employees whose offices are in Tokyo
    SELECT firstName, lastName, city FROM employees e
      JOIN offices o
      ON o.officeCode = e.officeCode
    WHERE city = "Tokyo";
 
 -- 15. Return the customer names of customers who were served by the employee named "Leslie Thompson"
   SELECT customerName FROM customers c
      JOIN employees e
      ON c.salesRepEmployeeNumber = e.employeeNumber
      WHERE e.firstName = "Leslie" AND e.lastName ="Thompson";
 -- 16. Return the product names and customer name of products ordered by "Baane Mini Imports"
   SELECT productName, customerName FROM customers c
      JOIN orders o
      ON c.customerNumber = o.customers;
      WHERE c.customerName = "Baane Mini Imports";

 -- 17. Return the employees' first names, employees' last names, customers' names, and offices' countries of transactions whose  --    customers and offices are in the same country.

   SELECT firstName, lastName customerName, country FROM employees e
      JOIN customers c
      ON e.employeeNumber = c.salesRepEmployeeNumber
      GROUP BY c.country;
 -- 18. Return the last names and first names of employees being supervised by "Anthony Bow".
  SELECT * FROM EMPLOYEES WHERE reportsTo = (SELECT employeeNumber FROM employees WHERE firstName = "ANTHONY" AND lastName = "BOW");
 -- 19. Return the product name and MSRP of the product with the highest MSRP.
    SELECT P.productName, P.MSRP FROM products P WHERE P.MSRP = (SELECT MAX(MSRP) FROM products)
 -- 20. Return the number of customers in the UK.
    SELECT COUNT(customerNumber) AS numberOfCustomer FROM customers WHERE country="UK";
 -- 21. Return the number of products per product line
    SELECT COUNT(productCode) AS numberOfProduct FROM products GROUP BY productLine;
 -- 22. Return the number of customers served by every employee
    SELECT COUNT(customer) AS numberOfCustomer FROM
 -- 23. Return the product name and quantity in stock of products that belong to the product line "planes" with stock quantities less than 1000
    SELECT p.productName, p.quantityInStock
    FROM products P
        LEFT JOIN productlines PL ON PL.PRODUCTLINE = P.productLine
    WHERE P.productLine = "PLANES" AND P.quantityInStock < 1000;
